# Composing PHP FPM + Nginx + MongoDB with Docker Compose


This project is example of simple implementation PHP FPM with Nginx Web server and connecting to mongoDB server in Docker, we separate configuration for deployment in Docker and source code

## Deployment in Docker

If you want deploy this application in docker, u can use command `docker-compose up` , this command will read file `docker-compose.yml` to creating container 

## Explanation of Directory and Files

This is important directory will be used in this project: 
> `server/etc/` : files conf for php-fpm and nginx\
> `server/entrypoint.sh` : files executed when container created\
> `Dockerfile` : file definition for build image in this project\
> `build.sh` : simple executable script to running build image and run container\
> `docker-compose.yml` : file definition for create container with different docker image, in this case will run PHP Application in this project and MongoDB Server

## References

To build this project, we use some references from several source like :

- https://github.com/elcobvg/lumen-realworld-example-app
- https://hub.docker.com/r/mvertes/alpine-mongo
- [Fixing install mongoDB php extension in Alpine Container](https://gist.github.com/yozik04/400b6bff8a98d1678a80fde6c8e102e9)
