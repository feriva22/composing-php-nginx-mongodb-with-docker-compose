#!/usr/bin/env sh
set -e

#php run migrate
php artisan key:generate
php artisan jwt:secret 
php artisan migrate:refresh
php artisan db:seed

php-fpm -D
nginx -g 'daemon off;'